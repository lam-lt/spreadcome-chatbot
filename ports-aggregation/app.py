import requests
import random
import utils
from pydantic import BaseModel
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


CONFIDENCE_THRESHOLD = 0.4

class Message(BaseModel):
    message: str

class Text(BaseModel):
    text: str

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/chatbot-model/train-and-replace")
def train_replace():
    '''
    Train a new model and replace the current model by the new one
    '''
    
    response = requests.get(url="http://rasa-core-server:5000/chatbot-model/train-and-replace")
    return response.json()

@app.post("/model/parse")
async def parse_message(text: Text):
    '''
    Send a text to the Rasa-server and get the parsed information
    '''

    url = "http://rasa-core-server:5005/model/parse"
    response = requests.post(url=url, data=text.json())
    return response.json()

@app.post("/webhooks/rest/webhook/")
async def send_message(message: Message):
    '''
    Send a message to the Rasa-server and get a response
    If the response's confidence < CONFIDENCE_THRESHOLD then return the default response
    '''
    
    # Default responses
    default_responses = [
        "大変申し訳ございません。理解できません。", 
        "大変申し訳ございません。理解できません。別の表現で質問して下さい。", 
        "大変申し訳ございません。理解できません。スタッフまでお問い合わせ下さい。",
        ]

    # If confidence < CONFIDENCE_THRESHOLD then return a random default response
    text = Text(text=message.message)
    confidence = utils.get_confidence(text=text)
    if confidence < CONFIDENCE_THRESHOLD:
        random_default_response = random.choice(default_responses)
        return [{"text": random_default_response, "confidence": confidence}]

    # If confidence >= CONFIDENCE_THRESHOLD then use the return of the model    
    url = "http://rasa-core-server:5005/webhooks/rest/webhook/"
    response = requests.post(url=url, data=message.json())
    return response.json()

if __name__ == "__main__":
    uvicorn.run("app:app", port=5001, host="0.0.0.0", reload=True)