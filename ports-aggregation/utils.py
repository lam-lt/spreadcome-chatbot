import requests
import random
from pydantic import BaseModel

class Text(BaseModel):
    text: str

def get_confidence(text: Text):
    '''
    Send a text to the Rasa-server and get the confidence
    '''

    url = "http://rasa-core-server:5005/model/parse"
    response = requests.post(url=url, data=text.json())
    confidence = response.json()["intent"]["confidence"]
    return confidence
