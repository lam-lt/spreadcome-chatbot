# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import re
from typing import Any, Text, Dict, List
import random
import string

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, FollowupAction, AllSlotsReset

food_data = [
    {'link': './static/img/たこ.jpeg', 'name': 'たこ', 'price': '100円'},
    {'link': './static/img/まぐろ.jpeg', 'name': 'まぐろ', 'price': '100円'},
    {'link': './static/img/赤えび.jpeg', 'name': '赤えび', 'price': '100円'},
    {'link': './static/img/まぐたく.jpeg', 'name': 'まぐたく', 'price': '100円'},
    {'link': './static/img/軍艦ねぎまぐろ.jpeg', 'name': '軍艦ねぎまぐろ', 'price': '100円'},
    {'link': './static/img/大吟醸.jpeg', 'name': '大吟醸', 'price': '100円'},
    {'link': './static/img/生貯蔵酒.jpeg', 'name': '生貯蔵酒', 'price': '100円'},
]

food_data_discount = [
    {'link': './static/img/オニオンサーモン.jpeg', 'name': 'オニオンサーモン', 'price': '90円'},
    {'link': './static/img/カニ風サラダ.jpeg', 'name': 'カニ風サラダ', 'price': '90円'},
    {'link': './static/img/たらこ.jpeg', 'name': 'たらこ', 'price': '90円'},
]

list_food = ['たこ', 'たらこ', 'まぐたく', 'まぐろ', 'オニオンサーモン', 'カニ風サラダ', '赤えび', '軍艦ねぎまぐろ']
list_drink = ['生貯蔵酒', '大吟醸']


class ActionAskForFood(Action):

    def name(self) -> Text:
        return "action_ask_for_food"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        food_response = {'text': '現在、ご提供できる料理はこちらの画面をご覧ください。', 'data': food_data}
        food_discount_response = {'text': 'その他、本日限定でお得な料理も楽しみに', 'data': food_data_discount}
        # dispatcher.utter_message()
        dispatcher.utter_message(json_message=food_response)
        dispatcher.utter_message(json_message=food_discount_response)

        return []


class ActionConfirmOrder(Action):

    def name(self) -> Text:
        return "action_confirm_order"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        user_input = tracker.latest_message.get("text")
        user_input = user_input.replace(' ', '')
        print('texttt:', user_input)
        # check quantity
        list_quantity = re.findall(r'\d+', user_input)

        list_order_food = tracker.get_slot("slot_order_food")
        list_order = []
        for f in list_food + list_drink:
            if f in user_input:
                # check quantity by condition: index of candidate quantity not better index of end string food add 2.
                f_end_idx = user_input.index(f) + len(f) - 1
                quantity_of_food = [int(i) for i in list_quantity if
                                    (user_input.index(i) <= f_end_idx + 2) & (user_input.index(i) > f_end_idx)]
                if quantity_of_food:
                    quantity_of_food = quantity_of_food[0]
                else:
                    quantity_of_food = 1
                list_order.append({'name': f, 'quantity': quantity_of_food})
        list_order_food += list_order

        # check drink in order food, yes or no
        check_has_drink = True
        for dr in list_drink:
            if dr in user_input:
                check_has_drink = False

        slots = []
        ask_discount = tracker.get_slot("slot_ask_discount")
        if ask_discount and check_has_drink:
            dispatcher.utter_message(response="utter_order_food")
            slots += [SlotSet("slot_ask_discount", False)]
        else:
            food_price_dict = {i['name']: int(i['price'].replace('円', '')) for i in food_data + food_data_discount}
            order_food_quant_dict = {i['name']: 0 for i in list_order_food}
            for of in list_order_food:
                order_food_quant_dict[of['name']] += of['quantity']
            list_order_food_distinct = [{'name': k, 'price': food_price_dict[k], 'quantity': v} for k, v in
                                        order_food_quant_dict.items()]

            format_bill = "<br>料理名----------価格----------数量 <br>"
            bill = map(
                lambda x: "{}----------{}円----------{}".format(x['name'], x['price'], x['quantity']),
                list_order_food_distinct)
            bill = "<br>".join(bill)
            format_bill += bill
            # dispatcher.utter_message(response="utter_confirm_order")
            dispatcher.utter_message(text=f"ご注文ありがとうございます。再度ご確認させていただきます。 {format_bill}")
        slots += [SlotSet("slot_order_food", list_order_food)]

        return slots


class ActionRefuse(Action):

    def name(self) -> Text:
        return "action_refuse"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        events = []
        list_order = tracker.get_slot("slot_order_food")
        if list_order:
            events += [FollowupAction(name="action_confirm_order")]
        else:
            dispatcher.utter_message(response="utter_thanks")

        return events


class ActionCreateOrderCode(Action):

    def name(self) -> Text:
        return "action_create_order_code"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        slots = [AllSlotsReset()]
        order_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
        dispatcher.utter_message(text=f"ありがとうございました。ご注文が完了しました。お客様のご注文番号は{order_code}です。")

        return slots


class ActionOrderFoodAgain(Action):

    def name(self) -> Text:
        return "action_order_food_again"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        slots = [SlotSet("slot_order_food", []), SlotSet("slot_ask_discount", True)]
        dispatcher.utter_message(response="utter_order_food_again")

        return slots
