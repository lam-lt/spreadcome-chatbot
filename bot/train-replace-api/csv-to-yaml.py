#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import json
import yaml
import os
import argparse


# In[2]:


parser = argparse.ArgumentParser()


# In[3]:


parser.add_argument('-s', '--source-file')
parser.add_argument('-t', '--target-folder')


# In[4]:


args = parser.parse_args()


# In[5]:


# type(args)


# In[6]:


# args.target_folder


# In[7]:


if not os.path.isdir(args.target_folder):
    raise Exception("{} is not the path".format(args.target_folder))
if not os.path.isfile(args.source_file):
    raise Exception("{} is not the file".format(args.source_file))


# In[ ]:





# In[ ]:





# In[8]:


input_csv = args.source_file


# In[9]:


faq_path = input_csv


# In[10]:


faq_df = pd.read_csv(faq_path)
faq_df


# In[ ]:





# In[11]:


#intents

intents = []

tmp = faq_df['intent'].to_list()
intents = [str(item) for item in tmp]
# intents


# In[ ]:





# In[12]:


#intents_responses

intents_responses = []

for index in range(0, len(faq_df)):
    series = faq_df.loc[index]
    intents_responses.append((str(series['intent']), series['response']))


# In[13]:


len(intents_responses)


# In[ ]:





# In[ ]:





# In[14]:


# questions_intents

questions_intents = []

nb_records = len(faq_df)
nb_fields = len(faq_df.loc[0])

for i in range(0, nb_records):
    for j in range(2, nb_fields):
        if not (faq_df.iloc[i,j] != faq_df.iloc[i,j]):
            questions_intents.append((faq_df.iloc[i,j], str(faq_df.iloc[i,0])))


# In[15]:


# len(questions_intents)


# In[ ]:





# In[ ]:





# ### Chuan hoa dau vao

# In[ ]:





# In[16]:


# intents


# In[17]:


# questions_intents


# In[18]:


# intents_responses


# In[ ]:





# In[19]:


nlu_input = {}
for item in intents:
    nlu_input[item] = []
    
# nlu_input


# In[20]:


for item in questions_intents:
    nlu_input[item[1]].append(item[0])

# nlu_input


# In[ ]:





# In[21]:


# stories
stories_input = [item for item in intents]
# stories_input


# In[22]:


# rules

rules_input = [item for item in intents]
# rules_input


# In[23]:


# domain


# In[ ]:





# In[ ]:





# ### List to Json

# #### NLU

# In[24]:


nlu_data = nlu_input


# In[25]:


res = {}

res['version'] = '3.1'

res['nlu'] = []
for key in nlu_data:
    examples = ''
    for example in nlu_data[key]:
        examples += '- ' + example + '\n'
    
    tmp = {'intent':key, 'examples':examples}
    
    res['nlu'].append(tmp)


# In[26]:


# res


# In[ ]:





# In[27]:


# tmp = json.dumps(res, ensure_ascii=False)
# tmp


# In[28]:


# res['nlu']


# In[29]:


# with open("FAQ/28-Nov/nlu.json", "w") as outfile:
#     json.dump(res, outfile, ensure_ascii=False)


# In[30]:


with open(os.path.join(args.target_folder, "nlu.json"), "w") as outfile:
    json.dump(res, outfile, ensure_ascii=False)


# #### Stories

# In[31]:


#input
# input_stories = ['pregnant', 'children', 'diabetes']
input_stories = stories_input


# In[32]:


stories = {}
stories['version'] = '3.1'
stories['stories'] = []


# In[33]:


stories['stories'] = [{
    'story': item + ' story',
    'steps': [{'intent': item}, {'action': 'utter_' + item}]
} for item in input_stories]


# In[34]:


# stories['stories']


# In[35]:


# stories


# In[36]:


with open(os.path.join(args.target_folder, "stories.json"), 'w') as f:
    json.dump(stories, f, ensure_ascii=False)


# In[ ]:





# #### rule

# In[37]:


input_rules = rules_input


# In[38]:


rules = {}
rules['version'] = '3.1'
rules['rules'] = []


# In[39]:


rules['rules'] = [{
    'rule': item + ' rule',
    'steps': [{'intent': item}, {'action': 'utter_' + item}]
} for item in input_rules]


# In[40]:


# rules


# In[41]:


with open(os.path.join(args.target_folder, "rules.json"), 'w') as f:
    json.dump(rules, f, ensure_ascii=False)


# In[ ]:





# #### domain

# In[42]:


# intents_responses


# In[43]:


res = {}

res['intents'] = [x[0] for x in intents_responses]
# print(res)


# In[44]:


# {'utter_'+(x[0]):[{'text':x[1]}] for x in intents_responses}


# In[45]:


res['responses'] = {'utter_'+(x[0]):[{'text':x[1]}] for x in intents_responses}


# In[46]:


res['version'] = '3.1'
res['session_config'] = {'session_expiration_time': 60, 'carry_over_slots_to_new_session': True}


# In[47]:


tmp = json.dumps(res, ensure_ascii=False)


# In[48]:


with open(os.path.join(args.target_folder, "domain.json"), 'w') as f:
    f.write(tmp)


# In[ ]:





# In[ ]:





# In[ ]:





# ### json to yaml

# In[49]:


with open(os.path.join(args.target_folder, "nlu.json"), 'r') as file:
    configuration = json.load(file)
    
with open(os.path.join(args.target_folder, "nlu.yml"), 'w') as yaml_file:
    yaml.safe_dump(configuration, yaml_file, allow_unicode=True)


# In[50]:


with open(os.path.join(args.target_folder, "rules.json"), 'r') as file:
    configuration = json.load(file)
    
with open(os.path.join(args.target_folder, "rules.yml"), 'w') as yaml_file:
    yaml.safe_dump(configuration, yaml_file, allow_unicode=True)


# In[51]:


with open(os.path.join(args.target_folder, "stories.json"), 'r') as file:
    configuration = json.load(file)
    
with open(os.path.join(args.target_folder, "stories.yml"), 'w') as yaml_file:
    yaml.safe_dump(configuration, yaml_file, allow_unicode=True)


# In[52]:


with open(os.path.join(args.target_folder, "domain.json"), 'r') as file:
    configuration = json.load(file)
    
with open(os.path.join(args.target_folder, "domain.yml"), 'w') as yaml_file:
    yaml.safe_dump(configuration, yaml_file, allow_unicode=True)


# In[ ]:
os.system("rm {}".format(os.path.join(args.target_folder, "*.json")))


# %%
