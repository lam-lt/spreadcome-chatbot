import os
# import subprocess
import requests
import pandas as pd


def fetch_and_convert():
    '''
    Fetch FAQs from database then convert them to csv format
    '''

    os.chdir("/train-replace-api/")

    response = requests.get('https://www.api.spreadcome.demo2.aimenext.com:6789/cms/public/faqs')
    response = response.json()

    data = response['data']['data']


    # Convert fetched FAQs into csv format
    faqs = []
    max_nb_samples = 0

    for item in data:
        intent_id = item['id']
        answer = item['answer']
        question = item['question']
        sample = item['sample']
        
        max_nb_samples = max(max_nb_samples, len(sample))
        
        faq = [intent_id, answer, question]
        for _sample in sample:
            faq.append(_sample)
        
        faqs.append(tuple(faq))

    header = ['intent', 'response', 'question']
    for index in range(max_nb_samples):
        header.append(str(index+1))

    df = pd.DataFrame(faqs, columns=header)
    df.to_csv("./tmp/fetched_faqs.csv", index=False)

def csv2yml():
    '''
    Convert csv format to yml format
    '''
    os.chdir("/train-replace-api/")
    # subprocess.run(["python", "csv-to-yaml.py", "-s", "tmp/fetched_faqs.csv", "-t", "yaml/"])
    os.system("python csv-to-yaml.py -s tmp/fetched_faqs.csv -t yaml/")

def train():
    os.chdir("/rasa/")
    os.system("python -m rasa train")