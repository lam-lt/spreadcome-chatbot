import requests
import logging
import os
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
# from fastapi.middleware.trustedhost import TrustedHostMiddleware
from utils import fetch_and_convert, csv2yml, train

os.chdir("/train-replace-api/")

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/pwd")
def pwd():
    cwd = os.getcwd()
    return {"Cwd": cwd}

@app.get("/chatbot-model/train-and-replace")
def train_and_replace():
    # fetch FAQs data and convert to yaml format
    print(">>>>>>>>>>>>>>>>>>>>>>>Fetch data and convert to YAML>>>>>>>>>>>>>>>>>>>>>>>")
    try:
        fetch_and_convert()
        csv2yml()
    except:
        print("loi cho nay :))")
        pass
    print("<<<<<<<<<<<<<<<<<<<<<<<Fetch data and convert to YAML<<<<<<<<<<<<<<<<<<<<<<<")


    # move yaml files to correct directory
    print(">>>>>>>>>>>>>>>>>>>>>>>Move YAML to correct directory>>>>>>>>>>>>>>>>>>>>>>>")
    os.system("pwd")
    os.system("cp ./yaml/nlu.yml /rasa/data/")
    os.system("cp ./yaml/rules.yml /rasa/data/")
    os.system("cp ./yaml/stories.yml /rasa/data/")
    os.system("cp ./yaml/domain.yml /rasa/")
    print("<<<<<<<<<<<<<<<<<<<<<<<Move YAML to correct directory<<<<<<<<<<<<<<<<<<<<<<<")

    # train a new model
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>Train model>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    train()
    print("<<<<<<<<<<<<<<<<<<<<<<<<<Train model<<<<<<<<<<<<<<<<<<<<<<<<<")

    # replace the old model
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Replace>>>>>>>>>>>>>>>>>>>>>>>>")
    list_models = os.listdir("/rasa/models/")
    list_models = sorted(list_models, reverse=True)
    latest_model = list_models[0]
    latest_model = os.path.join("/rasa/models/", latest_model)
    print(latest_model)
    payload='''{"model_file": ''' + '''"{}"'''.format(latest_model) + '''}'''
    print(payload)
    response = requests.put("http://127.0.0.1:5005/model", data=payload)
    print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Replace<<<<<<<<<<<<<<<<<<<<<<<<")

    return {"message":"trained successfully"}

if __name__ == "__main__":
    uvicorn.run("train_and_replace:app", port=5000, host="0.0.0.0", reload=True)